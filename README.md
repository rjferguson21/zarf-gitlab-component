# Zarf Package Create

Create a zarf package

## Usage

Use this component to scan your source code and get a report about code quality and complexity.

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: https://gitlab.com/defense-unicorns/gitlab-components/zarf-component/zarf-package-create@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

This adds a job called `zarf_package_create` to the pipeline.

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `path` | `.`      | The path to the directory where your zarf.yaml is located (Defaults to current working directory) |
| `stage` | `build`      | The stage where you want the job to be added |
| `zarf_image` | `curlimages/curl` | The Docker image  |

### Variables

You can customize the code quality analyzer by defining the following CI/CD variables:

| CI/CD variable | Description |
| -------------- | ----------- |
| `TMP_NOT_REAL` | Path to the source code to scan |

## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components 